(() => {
	'use strict';

	angular.module('app.filter').filter('url', $url => {
		return function(input, kind = 'url') {
			return $url.get(input, kind) || '';
		};
	});

	angular.module('app.filter').filter('imageUrl', $url => {
		return function(input) {
			return $url.imageUrl(input) || '';
		};
	});
})();
