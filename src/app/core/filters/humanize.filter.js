(() => {
	'use strict';

	angular.module('app.filter').filter('humanize', () => {
		return input => {
			const words = _.words(input);
			let humanized = '';
			words.forEach(word => {
				humanized += `${_.upperFirst(_.lowerCase(word))} `;
			});
			return _.trim(humanized);
		};
	});
})();
