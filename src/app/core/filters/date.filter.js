(() => {
	'use strict';

	angular.module('app.filter').filter('fecha', $filter => input => {
		return $filter('date')(input, `d 'de' MMMM, y`);
	});
})();
