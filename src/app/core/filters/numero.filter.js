(() => {
	'use strict';

	angular.module('app.filter').filter('numero', () => input => {
		if (input) {
			var n = input.toString(),
				p = n.indexOf(',');

			return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i) {
				return p < 0 || i < p ? $0 + '.' : $0;
			});
		}
	});
})();
