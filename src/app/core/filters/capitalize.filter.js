(() => {
	'use strict';

	angular
		.module('app.filter')
		.filter('capitalize', () => input => _.capitalize(input));
})();
