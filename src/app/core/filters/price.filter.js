(() => {
	'use strict';

	angular.module('app.filter').filter('price', () => input => {
		function format(number) {
			if (number) {
				const cleaned = number.toString().replace(/\D+/g, '');
				var num = cleaned ? cleaned.toString() : number;
				// console.log(/[1-9]\d*/g.exec(num));
				var n = num.toString(),
					p = n.indexOf(',');

				return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i) {
					return p < 0 || i < p ? $0 + '.' : $0;
				});
			}

			return 0;
		}

		return `$${format(parseInt(input))}`;
	});
})();
