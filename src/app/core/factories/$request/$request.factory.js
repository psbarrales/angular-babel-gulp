(() => {
	'use strict';

	angular
		.module('app.factory')
		.config($resourceProvider => {
			$resourceProvider.defaults.stripTrailingSlashes = false;
		})
		.provider('$request', $requestProvider);

	function $requestProvider() {
		const RESOURCES = {};
		const URLS = {};

		const TIMEOUT_SOCKET = 2500;

		return {
			add: add,
			$get: function($q, $http, $socket, $timeout, $localStorage) {
				const CACHE = $localStorage.getItem('CACHE_REQUEST') || {};
				const CACHE_EXPIRATION = $localStorage.getItem('CACHE_REQUEST_EXPIRATION') || {};

				const cache = {
					query: cacheQuery,
					get: cacheGet,
					plain: cachePlain
				};

				const socket = {
					query: socketQuery,
					get: socketGet,
					plain: socketPlain
				};

				return {
					query: query,
					plain: plain,
					get: get,
					post: post,
					save: post,
					update: update,
					patch: patch,
					remove: remove,
					delete: remove,
					endpoint: endpoint,
					http: http,
					cache: cache,
					socket: socket
				};

				function hash(name, params, method = 'GET') {
					return JSON.stringify({
						name: name,
						params: params,
						method: method
					}).encode();
				}

				function query(name, params, defer) {
					const deferred = defer || $q.defer();
					const hashed = hash(name, params);
					try {
						RESOURCES[name]
							.query(params)
							.$promise.then(result => {
								socketResponse(hashed, result);
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function get(name, params, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.get(params)
							.$promise.then(result => {
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function plain(name, params, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.plain(params)
							.$promise.then(result => {
								deferred.resolve(result.response);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function post(name, data, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.save(data)
							.$promise.then(result => {
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function update(name, data, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.update(data)
							.$promise.then(result => {
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function patch(name, data, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.patch(data)
							.$promise.then(result => {
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}
				function remove(name, params, defer) {
					const deferred = defer || $q.defer();
					try {
						RESOURCES[name]
							.remove(params)
							.$promise.then(result => {
								deferred.resolve(result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					} catch (e) {
						deferred.reject(e);
					}
					return deferred.promise;
				}

				function http(name, url = '', config = {}) {
					return $http(
						angular.extend(
							{},
							{
								url: URLS[name] + url,
								method: 'GET'
							},
							config
						)
					);
				}

				function endpoint(name) {
					return URLS[name];
				}

				function cacheQuery(name, params, defer) {
					const deferred = defer || $q.defer();
					const indexHashed = hash(name, params);

					if (getCache(indexHashed)) {
						deferred.resolve(getCache(indexHashed));
					} else {
						query(name, params, deferred).then(result => {
							saveCache(indexHashed, result);
						});
					}
					return deferred.promise;
				}

				function cacheGet(name, params, defer) {
					const deferred = defer || $q.defer();
					const indexHashed = hash(name, params);

					if (getCache(indexHashed)) {
						deferred.resolve(getCache(indexHashed));
					} else {
						get(name, params, deferred)
							.then(result => {
								deferred.resolve(result);
								saveCache(indexHashed, result);
							})
							.catch(err => {
								deferred.reject(err);
							});
					}
					return deferred.promise;
				}

				function cachePlain(name, params, defer) {
					const deferred = defer || $q.defer();
					const indexHashed = hash(name, params);

					if (getCache(indexHashed)) {
						deferred.resolve(getCache(indexHashed));
					} else {
						plain(name, params, deferred).then(result => {
							saveCache(indexHashed, result);
						});
					}
					return deferred.promise;
				}

				function getCache(index) {
					if (CACHE_EXPIRATION[index]) {
						const now = new Date();
						if ((now.getTime() - CACHE_EXPIRATION[index].getTime()) / (1000 * 60 * 60 * 24) <= 24) {
							return CACHE[index];
						} else {
							_.unset(CACHE_EXPIRATION, index);
							_.unset(CACHE, index);
							$localStorage.setItem('CACHE_REQUEST', CACHE);
							$localStorage.setItem('CACHE_REQUEST_EXPIRATION', CACHE_EXPIRATION);
							return false;
						}
					} else {
						return false;
					}
				}

				function saveCache(index, result) {
					const now = new Date();
					CACHE[index] = result;
					CACHE_EXPIRATION[index] = new Date(now.setHours(now.getHours() + 24));
					$localStorage.setItem('CACHE_REQUEST', CACHE);
					$localStorage.setItem('CACHE_REQUEST_EXPIRATION', CACHE_EXPIRATION);
				}

				function socketResponse(hashed, response) {
					$socket.on(`request.${hashed}`, () => {
						/* ESPERANDO PARA RESPONDER */
						$socket.emit('response', {
							hash: hashed,
							response: response
						});
					});
				}

				function socketQuery(name, params) {
					const deferred = $q.defer();
					const indexHashed = hash(name, params);
					let responsed = false;

					$socket.emit('request', { hash: indexHashed });
					$socket.on(`response.${indexHashed}`, response => {
						responsed = true;
						$socket.off(`response.${indexHashed}`);
						deferred.resolve(response);
					});

					/* ESPERA 3 SEG LA RESPUESTA */
					$timeout(() => {
						if (!responsed) {
							$socket.off(`response.${indexHashed}`);
							query(name, params, deferred);
						}
					}, TIMEOUT_SOCKET);
					return deferred.promise;
				}
				function socketGet(name, params) {
					const deferred = $q.defer();
					const indexHashed = hash(name, params);
					let responsed = false;

					$socket.emit('request', { hash: indexHashed });
					$socket.on(`response.${indexHashed}`, response => {
						responsed = true;
						$socket.off(`response.${indexHashed}`);
						deferred.resolve(response);
					});

					/* ESPERA 3 SEG LA RESPUESTA */
					$timeout(() => {
						if (!responsed) {
							$socket.off(`response.${indexHashed}`);
							get(name, params, deferred);
						}
					}, TIMEOUT_SOCKET);
					return deferred.promise;
				}
				function socketPlain(name, params) {
					const deferred = $q.defer();
					const indexHashed = hash(name, params);
					let responsed = false;

					$socket.emit('request', { hash: indexHashed });
					$socket.on(`response.${indexHashed}`, response => {
						responsed = true;
						$socket.off(`response.${indexHashed}`);
						deferred.resolve(response);
					});

					/* ESPERA 3 SEG LA RESPUESTA */
					$timeout(() => {
						if (!responsed) {
							$socket.off(`response.${indexHashed}`);
							plain(name, params, deferred);
						}
					}, TIMEOUT_SOCKET);
					return deferred.promise;
				}
			}
		};

		function add(name, url, params = {}) {
			const $resource = angular.injector(['ngResource']).get('$resource');
			URLS[name] = url;
			RESOURCES[name] = $resource(url, params, {
				plain: {
					action: 'plain',
					method: 'GET',
					transformResponse: data => {
						return { response: data };
					}
				},
				patch: { method: 'PATCH' },
				update: { method: 'PUT' }
			});
		}
	}
})();
