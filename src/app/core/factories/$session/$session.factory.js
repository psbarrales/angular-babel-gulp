(() => {
	angular.module('app.factory').provider('$session', SessionProvider);

	function SessionProvider() {
		const SESSION_ID = new Date().getTime().toString();
		return {
			$get: function() {
				return {
					get: get
				};

				function get() {
					return SESSION_ID;
				}
			}
		};
	}
})();
