(() => {
	'use strict';

	angular.module('app.factory').factory('$localStorage', () => {
		const storage = window.localStorage || window.sessionStorage;
		const Base64 = {
			encode: value => window.btoa(value),
			decode: value => window.atob(value)
		};

		return {
			setItem: setItem,
			getItem: getItem,
			removeItem: removeItem,
			clear: clear
		};

		function setItem(name, value) {
			const nameEncoded = Base64.encode(name);
			const valueEncoded = Base64.encode(angular.toJson(value));
			storage.setItem(nameEncoded, valueEncoded);
		}
		function getItem(name) {
			const nameEncoded = Base64.encode(name);
			const value = storage.getItem(nameEncoded);
			if (value) {
				return angular.fromJson(Base64.decode(value));
			} else {
				return null;
			}
		}
		function removeItem(name) {
			const nameEncoded = Base64.encode(name);
			storage.removeItem(nameEncoded);
		}
		function clear() {
			storage.clear();
		}
	});
})();
