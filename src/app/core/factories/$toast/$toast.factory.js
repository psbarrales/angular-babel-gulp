(() => {
	'use strict';

	angular
		.module('app.factory')
		.config($toastPresets)
		.factory('$toast', $toastFactory);

	function $toastPresets($mdToastProvider) {
		$mdToastProvider.addPreset('error', {
			options: function() {
				return {
					template:
						'<md-toast>' +
						'<div class="md-toast-content">' +
						'This is a custom preset' +
						'</div>' +
						'</md-toast>',
					controllerAs: 'toast',
					bindToController: true
				};
			}
		});
	}

	function $toastFactory($mdToast) {
		return {
			show: show,
			error: error,
			success: success
		};

		function show(
			message,
			time = 5000,
			position = 'top right',
			className = ''
		) {
			$mdToast.show(
				$mdToast
					.simple()
					.toastClass(className)
					.textContent(message)
					.position(position)
					.hideDelay(time)
			);
		}

		function error(message, time = 5000, position = 'top right') {
			show(message, time, position, 'toast-error');
		}

		function success(message, time = 5000, position = 'top right') {
			show(message, time, position, 'toast-success');
		}
	}
})();
