(() => {
	'use strict';

	angular.module('app.factory').provider('$steps', $stepsProvider);

	function $stepsProvider() {
		const steps = [];
		let currentStep = 1;

		return {
			add: add,
			$get: function() {
				const callbacks = [];

				return {
					get: get,
					change: change,
					current: current,
					onChange: onChange
				};

				function change(n) {
					currentStep = n;
					callbacks.forEach(cb => {
						cb(n);
					});
				}

				function current() {
					return currentStep;
				}

				function onChange(cb) {
					callbacks.push(cb);
				}
			}
		};

		function add(step) {
			steps.push(step);
		}

		function get() {
			return steps;
		}
	}
})();
