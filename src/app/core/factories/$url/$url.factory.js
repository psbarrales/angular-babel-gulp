(() => {
	'use strict';

	angular.module('app.factory').provider('$url', $urlProvider);

	function $urlProvider(CONFIGURATION) {
		const CDN = CONFIGURATION.ENDPOINTS.CDN;

		const URLS = [];
		return {
			add: add,
			$get: () => {
				return {
					get: get,
					imageUrl: imageUrl
				};

				function get(name, kind) {
					const URL = _.find(URLS, {
						id: name
					}).url;
					switch (kind) {
						case 'url':
							return `${CDN}${URL}`;
						case 'image':
							return `${CDN}/full-cobertura/img${URL}`;
						case 'campaign':
							return `${CDN}/full-cobertura/campaign${URL}`;
						case 'documents':
							return `${CDN}/full-cobertura/documents${URL}`;
					}
					return;
				}

				function imageUrl(image) {
					return `${CDN}/full-cobertura/img/${image}`;
				}
			}
		};

		function add(name, url) {
			URLS.push({
				id: name,
				url: `${url}`
			});
		}
	}
})();
