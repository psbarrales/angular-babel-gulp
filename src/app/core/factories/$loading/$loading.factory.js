/**
 * @name Loading
 * @class $loading
 * @memberOf App.Core.Factory
 * @description 
 * Muestra y oculta un <i>modal</i> con spinner <br />
 * - Permite ingreso de <i>message</i>
 */

(() => {
	'use strict';

	angular.module('app.factory').factory('$loading', $mdDialog => {
		return {
			show: show,
			hide: hide
		};

		/**
		 * @name show
		 * @description Muestra <i>modal</i>
		 * @param {Event} $event Evento click, permite que el modal salga desde el elemento
		 * @param {String} message Mensaje a mostrar en el modal cargando
		 * @memberOf App.Core.Factory.$loading
		 */
		function show($event, message) {
			const parentEl = angular.element(document.body);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: false,
				escapeToClose: false,
				disableParentScroll: true,
				template: `<md-dialog class="loading-dialog">
					   <md-dialog-content>
					       <div class="text-center">
					           <div class="spinner big-spinner"></div>
					           <div class="message">${message}</div>
					       </div>
					   </md-dialog-content>
					</md-dialog>`
			});
		}

		/**
		 * @name hide
		 * @description Oculta <i>modal</i>
		 * @memberOf App.Core.Factory.$loading
		 */
		function hide() {
			$mdDialog.hide();
		}
	});
})();
