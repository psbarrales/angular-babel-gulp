/**
 * @namespace Factory
 * @memberOf App.Core
 */

(() => {
	'use strict';

	angular.module('app.factory', ['app.configuration']);
})();
