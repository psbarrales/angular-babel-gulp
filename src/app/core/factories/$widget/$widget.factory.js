(() => {
	'use strict';

	angular.module('app.factory').factory('$widget', $widgetFactory);

	function $widgetFactory() {
		return {
			get: get
		};

		function build(name, attr = {}, className = [], $scope, weight = 1) {
			return {
				name: name,
				attr: attr,
				className: className,
				$scope: $scope,
				weight: weight
			};
		}

		function get(widgets) {
			if (angular.isString(widgets)) {
				return [ build(widgets) ];
			} else if (angular.isArray(widgets)) {
				return widgets.map(widget => {
					if (angular.isString(widget)) {
						return build(widget);
					} else {
						return build(
							widget.name,
							widget.attr,
							widget.className,
							widget.$scope,
							widget.weight
						);
					}
				});
			} else if (angular.isObject(widgets)) {
				return [
					build(
						widgets.name,
						widgets.attr,
						widgets.className,
						widgets.$scope,
						widgets.weight
					)
				];
			}
		}
	}
})();
