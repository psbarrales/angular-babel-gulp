(() => {
	'use strict';

	angular.module('app.factory').provider('$socket', function() {
		let server = '';
		return {
			server: url => {
				server = url;
			},
			$get: function($log) {
				const socket = io(server || 'http://psbarrales.com:4000');

				return {
					on: (event, cb) => {
						socket.on(event, cb);
					},
					once: (event, cb) => {
						socket.on(event, cb);
					},
					off: event => {
						socket.off(event);
					},
					emit: (event, values) => {
						socket.emit(event, values);
					}
				};
			}
		};
	});
})();
