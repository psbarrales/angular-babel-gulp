(() => {
	angular.module('app.factory').factory('$pubsub', PubSubFactory);

	function PubSubFactory() {
		const stores = {};
		window.STORES = stores;

		class Store {
			constructor(name, defaultValue) {
				this.state = defaultValue || {};
				this._subscribers = [];
				stores[name] = this;
			}

			subscribe(cb) {
				this._subscribers.push(cb);
			}
			dispatch(state) {
				this.state = state;
				this._subscribers.forEach(cb => {
					cb(this.state);
				});
			}
			getState() {
				return this.state;
			}
		}

		return {
			createStore: createStore,
			getStore: getStore
		};

		function createStore(name) {
			return stores[name] || new Store(name);
		}

		function getStore(name) {
			return stores[name] || null;
		}
	}
})();
