(() => {
	'use strict';

	angular
		.module('app.endpoints', [ 'app.factory', 'app.configuration' ])
		.config($resourceProvider => {
			$resourceProvider.defaults.stripTrailingSlashes = false;
		})
		.config((CONFIGURATION, $requestProvider) => {
			const ENDPOINTS = CONFIGURATION.ENDPOINTS;
			formsEndpoints($requestProvider, ENDPOINTS);
			actionsEndpoints($requestProvider, ENDPOINTS);
		})
		.config($urlProvider => {
			$urlProvider.add(
				'politicas_datos_personales',
				'/terminos-condiciones/1/politica-de-tratatamiento-informacion.pdf'
			);
			$urlProvider.add(
				'tratamiento_de_informacion',
				'/terminos-condiciones/1/tratamiento-de-informacion-asf.pdf'
			);
			$urlProvider.add(
				'terminos_y_condiciones',
				'/terminos-condiciones/campanias/terminos-y-condiciones-v2.jpg'
			);
		});

	function formsEndpoints($requestProvider, ENDPOINTS) {
		$requestProvider.add(
			'TipoDocumentos',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/tipodocumentos`
		);
		$requestProvider.add(
			'Marcas',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas`
		);
		$requestProvider.add(
			'Marca',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas/:marcaId`,
			{ marcaId: '@id' }
		);
		$requestProvider.add(
			'Modelos',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas/:marcaId/modelos`,
			{ marcaId: '@id' }
		);
		$requestProvider.add(
			'Modelo',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas/:marcaId/modelos/:modeloId`,
			{ marcaId: '@id', modeloId: '@id' }
		);
		$requestProvider.add(
			'Años',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas/:marcaId/modelos/:modeloId/anios`,
			{ marcaId: '@id', modeloId: '@id' }
		);
		$requestProvider.add(
			'Departamentos',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/departamentos`
		);
		$requestProvider.add(
			'Ciudades',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/departamentos/:departamentoId/ciudades`,
			{ departamentoId: '@id' }
		);
		$requestProvider.add(
			'Géneros',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/sexos`
		);
		$requestProvider.add(
			'0km',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/0km`
		);
		$requestProvider.add(
			'Paises',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/paises`
		);
		$requestProvider.add(
			'Nacionalidades',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/nacionalidades`
		);
		$requestProvider.add(
			'PrendaVehicular',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/prendavehicular`
		);
		$requestProvider.add(
			'FormCustom',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura`
		);
		$requestProvider.add(
			'Formulario',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/dform/cotizaciones/:cotizacionId/subproductos/:planId/formularios`,
			{ cotizacionId: '@id', planId: '@id' }
		);
	}

	function actionsEndpoints($requestProvider, ENDPOINTS) {
		$requestProvider.add('AntiguaExperiencia', `${ENDPOINTS.ANTIGUA}`);
		$requestProvider.add(
			'CotizarPlaca',
			`${ENDPOINTS.CLOUD}/colombia/webfc/v1/cotizacionplaca`
		);
		$requestProvider.add(
			'ValorAsegurado',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/form/marcas/:marcaId/modelos/:modeloId/anios/:anioId/valorasegurado`,
			{ marcaId: '@id', modeloId: '@id', anioId: '@id' }
		);
		$requestProvider.add(
			'Plan',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/planes/:planId`,
			{ planId: '@id' }
		);
		$requestProvider.add(
			'Propuesta',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/propuestas/ `
		);
		$requestProvider.add(
			'PropuestaResumen',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/propuestas/:propuestaId/resumen`,
			{ propuestaId: '@id' }
		);
		$requestProvider.add(
			'PropuestaMedioPago',
			`${ENDPOINTS.CLOUD}/colombia/full-cobertura/pago/propuesta/:propuestaId/mediopago`,
			{ propuestaId: '@id' }
		);
		$requestProvider.add(
			'Cotizaciones',
			`${ENDPOINTS.CLOUD}/colombia/webfc/v3/cotizaciones`
		);
		$requestProvider.add(
			'Cotizacion',
			`${ENDPOINTS.CLOUD}/colombia/webfc/v3/cotizaciones/:cotizacionId`,
			{ cotizacionId: '@id' }
		);
		$requestProvider.add(
			'Pagar',
			`${ENDPOINTS.CLOUD}/colombia/webfc/v3/pagos`
		);
		$requestProvider.add(
			'PagarStatus',
			`${ENDPOINTS.CLOUD}/colombia/restor/api/quotes/status`
		);
	}
})();
