/**
 * @namespace Core
 * @memberOf App
 */

(() => {
	'use strict';

	angular.module('app.core', [
		'ngResource',
		'app.endpoints',
		'app.component',
		'app.decorator',
		'app.factory',
		'app.filter',
		'app.layouts'
	]);
})();
