(() => {
	'use strict';

	angular.module('app.layouts', []).config($stateProvider => {
		$stateProvider.state('steps', {
			abstract: true,
			url: '/steps',
			templateUrl: 'app/core/layouts/templates/wizard.tmpl.html'
		});
		$stateProvider.state('pago', {
			abstract: true,
			url: '/pago',
			templateUrl: 'app/core/layouts/templates/simple.tmpl.html'
		});
	});
})();
