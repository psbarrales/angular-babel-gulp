(() => {
	'use strict';

	angular.module('app.component').component('loading', {
		template: '<div class="spinner"></div><div class="message" ng-if="$ctrl.message">{{$ctrl.message}}</div>',
		bindings: {
            show: '=',
            message: '@'
		}
	});
})();
