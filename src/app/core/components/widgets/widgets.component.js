(() => {
	'use strict';

	angular.module('app.component').component('widgets', {
		restrict: 'E',
		template: '',
		controller: WigetsController,
		bindings: {
			ngWidgets: '='
		}
	});

	function WigetsController($sce, $element, $rootScope, $compile) {
		const $ctrl = this;
		$ctrl.$onChanges = function() {
			compileWidgets();
		};

		function compileWidgets() {
			$ctrl.ngWidgets.forEach(widget => {
				let className = 'widget';
				let attributes = '';
				widget.className.forEach(cName => {
					className += ` ${cName}`;
				});
				Object.keys(widget.attr).forEach(attr => {
					attributes += `${attr}="${widget.attr[attr]}"`;
				});
				const widgetCompiled = $compile(
					`<widget-${widget.name} class="${className}" ${attributes}></widget-${widget.name}>`
				)(widget.$scope || $rootScope);

				$element.append(widgetCompiled);
			});
		}
	}
})();
