(() => {
	'use strict';

	angular.module('app.component').component('mdInputLine', {
		restrict: 'E',
		templateUrl:
			'app/core/components/md-input-line/md-input-line.tmpl.html',
		transclude: true,
		controller: MdInputLineController,
		bindings: {
			placeholder: '@',
			loading: '='
		}
	});

	function MdInputLineController($scope, $element, $transclude) {
		this.$postLink = () => {
			$transclude(function(transEl, transScope) {
				$scope = transScope;
				$element.prepend(transEl);
			});
		};

		this.$doCheck = () => {
			if (this.readOnly) {
				$element.addClass('read-only');
			} else {
				$element.removeClass('read-only');
			}
			$scope.loading = this.loading;
			if (this.loading) {
				$element.addClass('loading');
				$element.find('input').attr('disabled', true);
			} else {
				$element.removeClass('loading');
				$element.find('input').attr('disabled', false);
			}
		};
	}
})();
