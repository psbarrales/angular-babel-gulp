(() => {
	'use strict';

	angular.module('app.component').component('content', {
		template: '<ng-transclude flex="1" ></ng-transclude>',
		controller: ContentController,
		transclude: true
	});

	function ContentController() {}
})();
