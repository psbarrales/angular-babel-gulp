(() => {
	'use strict';

	angular.module('app.component').directive('inputPhone', () => {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				inputNumber: '='
			},
			link: ($scope, e, a, ngModel) => {
				function format(number) {
					const cleaned = number.toString().replace(/\D+/g, '');
					var num = cleaned ? cleaned.toString() : number;
					// console.log(/[1-9]\d*/g.exec(num));
					var n = num.toString();

					return n.replace(/(\d{3})(\d+)/g, '$1-$2');
				}

				function validateNumber(_value) {
					return /^[1-9]\d*$/.test(_value.toString());
				}

				function cleanNumber(_value) {
					return _value ? _value.replace(/-/g, '') : '';
				}

				function addValidatorToNgModel(ngModel) {
					var validate = function(value) {
						const valid =
							value.length > 0 ? validateNumber(value) : true;
						ngModel.$setValidity('number', valid);
						return valid;
					};

					var validateAndFilter = function(_value) {
						_value = cleanNumber(_value);
						return validate(_value) ? _value : null;
					};

					var validateAndFormat = function(_value) {
						_value = cleanNumber(_value);
						validate(_value);
						return format(_value);
					};

					ngModel.$parsers.unshift(validateAndFilter);
					ngModel.$formatters.unshift(validateAndFormat);
				}

				function formatNumberOnWatch($scope, ngModel) {
					$scope.$watch(
						function() {
							return ngModel.$viewValue;
						},
						function() {
							ngModel.$setViewValue(format(ngModel.$viewValue));
							ngModel.$render();
						}
					);
				}

				addValidatorToNgModel(ngModel);
				formatNumberOnWatch($scope, ngModel);
			}
		};
	});
})();
