(() => {
	'use strict';

	angular.module('app.component').component('stepsWizard', {
		templateUrl: 'app/core/components/steps-wizard/steps-wizard.tmpl.html',
		controller: StepsWizardController
	});

	function StepsWizardController($log, $state, $steps) {
		const $ctrl = this;

		$ctrl.steps = $steps.get();
		$ctrl.step = $steps.current();
		$ctrl.changeStep = changeStep;

		function changeStep(step) {
			$ctrl.step = step.weight;
			$steps.change(step.weight);
		}

		$steps.onChange(() => {
			const current = $steps.current();
			const state = $state.$current.name;
			$ctrl.step = current;
			const step = _.find($ctrl.steps, { weight: current });
			if (step) {
				if (angular.isString(step.state)) {
					if (state !== step.state) {
						$state.go(step.state);
					}
				} else if (
					angular.isArray(step.state) &&
					step.state.length > 0
				) {
					if (step.state.indexOf(state) < 0) {
						$state.go(_.first(step.state));
					}
				}
				// $state.go(step.state);
			}
		});
	}
})();
