(() => {
	'use strict';

	angular.module('app.component').directive('inputUppercase', () => {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: ($scope, e, a, ngModel) => {
				function formatNumberOnWatch($scope, ngModel) {
					$scope.$watch(
						function() {
							return ngModel.$viewValue;
						},
						function() {
							if (ngModel.$viewValue) {
								ngModel.$setViewValue(
									ngModel.$viewValue.toUpperCase()
								);
							}
							ngModel.$render();
						}
					);
				}

				formatNumberOnWatch($scope, ngModel);
			}
		};
	});
})();
