(() => {
	'use strict';

	angular.module('app.component').component('mdAutocompleteCustom', {
		require: {
			ngModel: '^ngModel'
		},
		restrict: 'E',
		templateUrl:
			'app/core/components/md-autocomplete-custom/md-autocomplete-custom.tmpl.html',
		transclude: true,
		bindings: {
			ngValue: '@',
			items: '=',
			placeholder: '@',
			loading: '='
		},
		controller: MdAutocompleteCustomController
	});

	function MdAutocompleteCustomController(
		$log,
		$q,
		$scope,
		$element,
		$transclude
	) {
		const $ctrl = this;
		$ctrl.querySearch = querySearch;
		$ctrl.selectedItemChange = selectedItemChange;
		$ctrl.searchTextChange = searchTextChange;
		$ctrl.getPathShow = getPathShow;

		$ctrl.$onChanges = () => {
			$ctrl.ngModel.$viewChangeListeners.push(value => {
				console.log('$parsers', value);
				console.log($ctrl.ngModel);
				return value;
			});
			$ctrl.ngModel.$viewChangeListeners.push(value => {
				console.log('$viewChangeListeners', value);
				console.log($ctrl.ngModel);
				return value;
			});
		};

		$ctrl.$postLink = () => {
			$transclude(transEl => {
				// $scope = transScope;
				$element.prepend(transEl);
			});
		};

		function searchTextChange(text) {
			if (!text) {
				$ctrl.ngModel.$modelValue = null;
			}
		}

		function selectedItemChange(item) {
			$ctrl.ngModel.$setViewValue(item);
			$ctrl.ngModel.$render();
		}

		function querySearch(query) {
			const deferred = $q.defer();

			if ($ctrl.items) {
				var results = query
					? $ctrl.items.filter(createFilterFor(query))
					: $ctrl.items;
				deferred.resolve(results);
			} else {
				deferred.reject($ctrl.emptyMessage);
			}
			return deferred.promise;
		}

		function getPathShow(item) {
			if ($ctrl.ngValue) {
				return _.get(item, $ctrl.ngValue);
			} else {
				return item;
			}
		}

		function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);

			return function filterFn(item) {
				return (
					angular.lowercase(item.valor).indexOf(lowercaseQuery) === 0
				);
			};
		}
	}
})();
