(() => {
	'use strict';

	angular.module('app.component').component('mdSelectCustom', {
		restrict: 'E',
		templateUrl:
			'app/core/components/md-select-custom/md-select-custom.tmpl.html',
		transclude: true,
		bindings: {
			ngValue: '=',
			placeholder: '@',
			readOnly: '=',
			loading: '='
		},
		controller: MdSelectCustomController
	});

	function MdSelectCustomController($scope, $element, $transclude) {
		this.$postLink = () => {
			$transclude((transEl, transScope) => {
				$scope = transScope;
				$element.prepend(transEl);
			});
		};

		this.$doCheck = () => {
			if (this.readOnly) {
				$element.addClass('read-only');
			} else {
				$element.removeClass('read-only');
			}

			if (this.loading) {
				$element.addClass('loading');
			} else {
				$element.removeClass('loading');
			}
		};
	}
})();
