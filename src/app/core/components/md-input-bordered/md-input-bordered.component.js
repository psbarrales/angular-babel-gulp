(() => {
	'use strict';

	angular.module('app.component').component('mdInputBordered', {
		restrict: 'E',
		templateUrl:
			'app/core/components/md-input-bordered/md-input-bordered.tmpl.html',
		transclude: true,
		controller: MdInputBorderedController,
		bindings: {
			placeholder: '@',
			loading: '='
		}
	});

	function MdInputBorderedController($scope, $element, $transclude) {
		this.$postLink = () => {
			$transclude(function(transEl, transScope) {
				$scope = transScope;
				$element.prepend(transEl);
			});
		};

		this.$doCheck = () => {
			if (this.readOnly) {
				$element.addClass('read-only');
			} else {
				$element.removeClass('read-only');
			}
			if (this.loading) {
				$element.addClass('loading');
				$element.find('input').attr('disabled', true);
			} else {
				$element.removeClass('loading');
				$element.find('input').attr('disabled', false);
			}
		};
	}
})();
