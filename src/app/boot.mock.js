window.mockingModules = function() {
	angular.module('app.configuration', []).constant('CONFIGURATION', {
		KEYSTORAGE: 'PROD_KEY',
		ENDPOINTS: {
			API:
				'https://api-autofullcobertura-co-test.aseseguros-test.p.azurewebsites.net',
			CLOUD: 'https://api.staging.segurosfalabella.cloud'
		}
	});
};
