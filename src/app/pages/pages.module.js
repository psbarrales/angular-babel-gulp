/**
 * @namespace App.Pages
 * @memberOf App
 */
(() => {
	'use strict';

	angular.module('app.pages', ['app.core']).config($urlRouterProvider => {
		$urlRouterProvider.otherwise('/steps/step-1');
	});
})();
