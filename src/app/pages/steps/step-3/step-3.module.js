(() => {
	'use strict';

	angular
		.module('app.pages')
		.config($stepsProvider => {
			$stepsProvider.add({
				weight: 3,
				title: 'Compra',
				description: 'Datos de la compra',
				state: ['steps.step-3', 'steps.pago']
			});
		})
		.config($stateProvider => {
			$stateProvider.state('steps.step-3', {
				url: '/step-3',
				views: {
					'': {
						controller: 'StepThreeController',
						controllerAs: 'vm',
						templateUrl: 'app/pages/steps/step-3/step-3.tmpl.html'
					}
				}
			});
		});
})();
