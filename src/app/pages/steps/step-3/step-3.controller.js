/**
 * @class StepThreeController
 * @description
 * <pre>
 * <b>Route: </b>/step-3
 * <b>State: </b>steps.step-3
 * <b>Template: </b>app/pages/steps/step-3/step-3.tmpl.html</pre>
 * @memberOf App.Pages
 */
(() => {
	'use strict';

	angular.module('app.pages').controller('StepThreeController', StepThreeController);

	function StepThreeController($log, $pubsub, $request, $scope, $state, $steps, $toast, $widget) {
		/* CONSTANT */
		const vm = this;
		const STATE_NAME = $state.$current.name;
		const WIDGETS = [
			{
				name: 'detalle-plan',
				className: ['']
			}
		];

		const FORMULARIO = [
			{
				alias: 'ASEGURADO',
				title: 'Datos del Asegurado'
			},
			{
				alias: 'MATERIAASEGURADA',
				title: 'Datos Materia Asegurada'
			}
		];

		/* PUBLIC */
		vm.widgets = $widget.get(WIDGETS);
		vm.formularioConfg = FORMULARIO;

		/* STORE */
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');
		const PLAN_STORE = $pubsub.createStore('plan');
		const FORMULARIO_STORE = $pubsub.createStore('formularios');
		const FORMDINAMICO_STORE = $pubsub.createStore('formularioDinamico');
		const PROPUESTA_STORE = $pubsub.createStore('propuesta');
		FORMDINAMICO_STORE.subscribe(campos => {
			submitPropuesta(campos);
		});

		/* EVENTS */
		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name === STATE_NAME) {
				onInit();
			}
		});

		/**
		 * @name onInit
		 * @description
		 * Callback del evento <code>$stateChangeSuccess</code>
		 * @memberOf App.Pages.StepThreeController
		 */
		function onInit() {
			if ($steps.current() !== 3) {
				$steps.change(3);
			}
			getFormulario();
		}

		function getFormulario() {
			const cotizacion = COTIZACION_STORE.getState();
			const plan = PLAN_STORE.getState();
			if (cotizacion.externalId && plan.subProducto) {
				vm.loading = true;
				$request
					.query('Formulario', {
						cotizacionId: COTIZACION_STORE.getState().externalId,
						planId: PLAN_STORE.getState().subProducto.id
					})
					.then(formularios => {
						FORMULARIO_STORE.dispatch(formularios);
					})
					.catch(err => {
						$log.error(err);
					})
					.finally(() => {
						vm.loading = false;
					});
			}
		}

		function submitPropuesta(campos) {
			if (!vm.sendPropuesta) {
				vm.sendPropuesta = true;
				vm.loading = true;
				const formulario = {
					cotizacion: COTIZACION_STORE.getState().externalId,
					subproducto: PLAN_STORE.getState().subProducto.id,
					campos: campos
				};
				$request
					.post('Propuesta', formulario)
					.then(propuesta => {
						PROPUESTA_STORE.dispatch(propuesta);
						$state.go('steps.pago');
					})
					.catch(err => {
						$log.error(err);
						$toast.error('No ha sido posible generar la propuesta');
					})
					.finally(() => {
						vm.loading = false;
						vm.sendPropuesta = false;
					});
			}
		}
	}
})();
