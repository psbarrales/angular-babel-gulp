(() => {
	'use strict';

	angular.module('app.pages').controller('CotizaViewController', CotizaViewController);

	function CotizaViewController($log, $loading, $mdDialog, $pubsub, $request, $socket, $scope, $state, $stepOne, $toast) {
		/* CONSTANT */
		const vm = this;
		const STATE_NAME = $state.$current.name;

		/* STORES */
		const VIEWS_STORE = $pubsub.createStore('views');
		const ASEGURADO_STORE = $pubsub.createStore('asegurado');
		const COTIZAPLACA_STORE = $pubsub.createStore('cotizaplaca');
		const FORM_COTIZA_STORE = $pubsub.createStore('formCotiza');

		/* PUBLIC */
		vm.showFormasPago = showFormasPago;
		vm.getCotizarPlaca = getCotizarPlaca;
		vm.toHideStep = toHideStep;

		/* PRIVATE */
		VIEWS_STORE.subscribe(nStep => {
			vm.hideStep = nStep === 1 || !nStep ? false : true;
		});

		function onInit() {
			// vm.hideStep = false;
			vm.asegurado = FORM_COTIZA_STORE.getState() || {};
			vm.form = {
				loading: true
			};
			ASEGURADO_STORE.subscribe(asegurado => {
				vm.asegurado = asegurado || {};
			});

			getTiposDocumentos();
		}

		function getTiposDocumentos() {
			$request.socket
				.query('TipoDocumentos')
				.then(tiposDocumento => {
					vm.form.tiposDocumento = _.sortBy(tiposDocumento, tDoc => {
						switch (tDoc.id) {
							case '3':
								return 1;
							case '2':
								return 2;
							case '1':
								return 3;
						}
					});
					vm.asegurado.tipoDocumento = _.find(vm.form.tiposDocumento, {
						id: '1'
					});
				})
				.catch(err => {
					$log.error(err);
					$toast.error('No se logró obtener los tipos de documentos');
				})
				.finally(() => {
					vm.form.loading = false;
				});
		}

		/* WATCHER */
		$scope.$watch('vm.asegurado.placa', (nVal, oVal) => {
			if (nVal && nVal.toUpperCase() !== oVal) {
				vm.asegurado.placa = vm.asegurado.placa.toUpperCase();
			}
		});

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (STATE_NAME == state.name) {
				onInit();
			}
		});

		/* PUBLIC  */
		function showFormasPago($event) {
			const parentEl = angular.element(document.body);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: true,
				escapeToClose: true,
				disableParentScroll: true,
				template:
					'<md-dialog class="formas-pagos-dialog">' +
					'  <md-dialog-content>' +
					'<ul>' +
					'<li><span>Paga la primera cuota y las siguientes 11 te las cobramos mes a mes.</span></li>' +
					'<li><span>Paga con tu CMR Banco Falabella sin interes.</span></li>' +
					'<li><span>Recuerda que las tarjetas CMR corresponden a la franquicia Mastercard.</span></li>' +
					'</ul>' +
					'<div class="image-wrapper"><img ng-src="{{\'medios-pago.png\' | imageUrl}}" /></div>' +
					'  </md-dialog-content>' +
					'</md-dialog>'
			});
		}

		function getCotizarPlaca($event) {
			ASEGURADO_STORE.dispatch(vm.asegurado);
			COTIZAPLACA_STORE.dispatch({});
			if (vm.asegurado.placa) {
				$loading.show($event, 'Buscando información, espera por favor');
				$request
					.get('CotizarPlaca', {
						idDocumento: vm.asegurado.dni,
						placa: vm.asegurado.placa
					})
					.then(response => {
						vm.hideStep = true;
						response.aseguradoInput = vm.asegurado;
						COTIZAPLACA_STORE.dispatch(response);
						VIEWS_STORE.dispatch(2);
					})
					.catch(() => {
						vm.hideStep = true;
						COTIZAPLACA_STORE.dispatch({
							vehiculo: {},
							asegurado: {},
							aseguradoInput: vm.asegurado
						});
						VIEWS_STORE.dispatch(2);
					})
					.finally(() => {
						$loading.hide();
					});
			} else if (vm.asegurado.dni) {
				vm.hideStep = true;
				VIEWS_STORE.dispatch(2);
				COTIZAPLACA_STORE.dispatch({
					vehiculo: {},
					asegurado: {},
					aseguradoInput: vm.asegurado
				});
			}
		}

		function toHideStep() {
			vm.hideStep = false;
			VIEWS_STORE.dispatch(1);
		}
	}
})();
