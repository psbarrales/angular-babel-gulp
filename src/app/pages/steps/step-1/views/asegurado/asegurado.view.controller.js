(() => {
	'use strict';

	angular.module('app.pages').controller('AseguradoViewController', AseguradoViewController);

	function AseguradoViewController($log, $mdDialog, $request, $scope, $state, $steps, $pubsub, $toast) {
		/* CONSTANT */
		const vm = this;
		const STATE_NAME = $state.$current.name;

		/* STORES */
		const COTIZAPLACA_STORE = $pubsub.createStore('cotizaplaca');
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');
		const PARACOTIZAR_STORE = $pubsub.createStore('paraCotizar');
		const INFORMACIONVEHICULO_STORE = $pubsub.createStore('informacionVehiculo');
		const LOADING_STORE = $pubsub.createStore('loading');
		const ASEGURADO_STORE = $pubsub.createStore('asegurado');

		/* PUBLIC */
		vm.doCotizar = doCotizar;

		COTIZAPLACA_STORE.subscribe(cotizaPlaca => {
			if (cotizaPlaca && cotizaPlaca.asegurado) {
				vm.cotizaPlaca = cotizaPlaca;
				doCompletarDatosAsegurado();
			} else {
				doLimpiarDatosAsegurado();
			}
		});
		ASEGURADO_STORE.subscribe(asegurado => {
			vm.asegurado = asegurado || {};
		});

		/* PRIVATE */
		function onInit() {
			vm.form = {};

			getGeneros();
			getPaises();
			getNacionalidades();
		}

		function getGeneros() {
			$request.socket
				.query('Géneros')
				.then(generos => {
					vm.form.generos = generos;
					doCompletarDatosAsegurado();
				})
				.catch(err => {
					$log.error(err);
					$toast.error('No es posible obtener los géneros');
				});
		}

		function getPaises() {
			$request.socket
				.query('Paises')
				.then(paises => {
					vm.form.paises = _.sortBy(paises, pais => {
						return pais.valor.toLowerCase().indexOf('colomb') >= 0 ? 1 : 2;
					});
					doCompletarDatosAsegurado();
				})
				.catch(err => {
					$log.error(err);
					$toast.error('No es posible obtener los paises');
				});
		}

		function getNacionalidades() {
			$request.socket
				.query('Nacionalidades')
				.then(nacionalidades => {
					vm.form.nacionalidades = _.sortBy(nacionalidades, nacionalidad => {
						return nacionalidad.valor.toLowerCase().indexOf('colomb') >= 0 ? 1 : 2;
					});
					doCompletarDatosAsegurado();
				})
				.catch(err => {
					$log.error(err);
					$toast.error('No es posible obtener las nacionalidades');
				});
		}

		function doCompletarDatosAsegurado() {
			if (vm.cotizaPlaca && vm.cotizaPlaca.asegurado) {
				const asegurado = vm.cotizaPlaca.asegurado;
				/* GENERO */
				if (asegurado && asegurado.sexo) {
					asegurado.sexo = _.find(vm.form.generos, {
						id: asegurado.sexo.id
					});
				}
				/* PAIS */
				if (asegurado && asegurado.pais) {
					asegurado.pais = _.find(vm.form.paises, {
						id: asegurado.pais.id
					});
				}
				/* NACIONALIDAD */
				if (asegurado && asegurado.nacionalidad) {
					asegurado.nacionalidad = _.find(vm.form.nacionalidades, {
						id: asegurado.nacionalidad.id
					});
				}
				Object.keys(asegurado).forEach(key => {
					if (asegurado[key] === 'null') {
						asegurado[key] = null;
					}
				});
				vm.asegurado = asegurado;
			}
		}

		function doLimpiarDatosAsegurado() {
			vm.asegurado = {};
		}

		function cleanObject(o) {
			_.unset(o, '$$hashKey');
			_.unset(o, '$$mdSelectId');
			return o;
		}

		function cotizar() {
			const paraCotizar = PARACOTIZAR_STORE.getState();
			LOADING_STORE.dispatch(true);
			$request
				.save('Cotizaciones', paraCotizar)
				.then(result => {
					COTIZACION_STORE.dispatch(result);
					$steps.change(2);
				})
				.catch(err => {
					$log.error(err);
					$toast.error('No ha sido posible cotizar, Intente más tarde');
				})
				.finally(() => {
					loadingDialog.hide();
					LOADING_STORE.dispatch(false);
				});
		}

		const loadingDialog = {
			show: $event => {
				const parentEl = angular.element(document.body);
				$mdDialog.show({
					parent: parentEl,
					targetEvent: $event,
					clickOutsideToClose: false,
					escapeToClose: false,
					disableParentScroll: true,
					template:
						'<md-dialog class="loading-dialog">' +
						'   <md-dialog-content>' +
						'       <div class="text-center">' +
						'           <div class="spinner big-spinner"></div>' +
						'           <div class="message">Buscando información, espera por favor</div>' +
						'       </div>' +
						'   </md-dialog-content>' +
						'</md-dialog>'
				});
			},
			hide: () => {
				$mdDialog.hide();
			}
		};

		/* WATCHERS */
		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (STATE_NAME === state.name) {
				onInit();
			}
		});

		/* PUBLIC */
		function doCotizar($event) {
			const paraCotizar = {};
			LOADING_STORE.dispatch(true);
			/* Cotización Placa */
			const cotizaPlaca = COTIZAPLACA_STORE.getState();
			/* Información Asegurado */
			paraCotizar.informacionAsegurado = {
				/* - Datos contacto */
				email: vm.asegurado.email,
				fonoMovil: vm.asegurado.fonoMovil,
				fonoFijo: vm.asegurado.fonoFijo,
				/* - Información personal */
				identificador: cotizaPlaca.aseguradoInput.dni,
				nacimiento: vm.asegurado.nacimiento,
				nacionalidad: cleanObject(vm.asegurado.nacionalidad),
				nombre: vm.asegurado.nombre,
				primerApellido: vm.asegurado.primerApellido,
				segundoApellido: vm.asegurado.segundoApellido,
				sexo: cleanObject(vm.asegurado.sexo),
				pais: cleanObject(vm.asegurado.pais),
				tipoDocumento: cleanObject(cotizaPlaca.aseguradoInput.tipoDocumento)
			};

			/* Información Vehículo */
			const informacionVehiculo = INFORMACIONVEHICULO_STORE.getState();
			$log.debug('informacionVehiculo', informacionVehiculo);
			paraCotizar.informacionVehiculo = {
				anioModelo: cleanObject(informacionVehiculo.anioModelo),
				ciudad: cleanObject(informacionVehiculo.ciudad),
				departamento: cleanObject(informacionVehiculo.departamento),
				marca: cleanObject(informacionVehiculo.marca),
				modelo: cleanObject(informacionVehiculo.modelo),
				placa: cotizaPlaca.aseguradoInput.placa,
				prendaVehicular: cleanObject(informacionVehiculo.prendaVehicular),
				vehiculo0km: cleanObject(informacionVehiculo.cerokm),
				motor: cotizaPlaca.vehiculo.motorId,
				vin: cotizaPlaca.vehiculo.vin,
				/* Valor asegurado */
				valAsegurado: parseInt(informacionVehiculo.valorAsegurado),
				valAseguradoMax: informacionVehiculo.valor.aseguradoMax,
				valAseguradoMin: informacionVehiculo.valor.aseguradoMin,
				valAseguradoOtorgado: informacionVehiculo.valor.asegurado
			};
			/* Politicas */
			paraCotizar.politicas = vm.asegurado.politicas;
			PARACOTIZAR_STORE.dispatch(paraCotizar);
			cotizar($event);
		}
	}
})();
