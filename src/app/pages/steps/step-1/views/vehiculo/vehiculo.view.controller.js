(() => {
	'use strict';

	angular.module('app.pages').controller('VehiculoViewController', VehiculoViewController);

	function VehiculoViewController($log, $request, $scope, $state, $pubsub, $mdDialog, $toast) {
		/* CONSTANT */
		const vm = this;
		const STATE_NAME = $state.$current.name;

		/* STORES */
		const VIEWS_STORE = $pubsub.createStore('views');
		const COTIZAPLACA_STORE = $pubsub.createStore('cotizaplaca');
		const INFORMACIONVEHICULO_STORE = $pubsub.createStore('informacionVehiculo');

		/* PUBLIC */
		vm.toHideStep = toHideStep;
		vm.validaContinuaAsegurado = validaContinuaAsegurado;
		vm.showAyudaReferencia = showAyudaReferencia;

		function toHideStep() {
			vm.hideStep = false;
			VIEWS_STORE.dispatch(2);
		}

		function validaContinuaAsegurado() {
			INFORMACIONVEHICULO_STORE.dispatch(vm.vehiculo);
			VIEWS_STORE.dispatch(3);
		}

		function showAyudaReferencia($event) {
			const parentEl = angular.element(document.body);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: true,
				escapeToClose: true,
				disableParentScroll: true,
				templateUrl: 'app/pages/steps/step-1/modals/ayuda-referencia.modal.tmpl.html',
				controller: function($scope, $mdDialog) {
					$scope.close = () => {
						$mdDialog.hide();
					};
				}
			});
		}

		/* WATCHER */
		VIEWS_STORE.subscribe(nStep => {
			vm.hideStep = nStep === 2 ? false : true;
		});

		COTIZAPLACA_STORE.subscribe(cotizaPlaca => {
			if (cotizaPlaca && cotizaPlaca.vehiculo) {
				vm.vehiculo = cotizaPlaca.vehiculo;
				assignVehiculo('all');
				setReadOnly(cotizaPlaca.vehiculo);
			}
		});

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name === STATE_NAME) {
				onInit();
			}
		});

		$scope.$watch('vm.vehiculo.marca', nVal => {
			if (nVal) {
				vm.form.modelos = [];
				vm.form.aniosModelo = [];
				query('Modelos', 'modelos', {
					marcaId: nVal.id
				});
			}
		});

		$scope.$watch('vm.vehiculo.modelo', nVal => {
			if (nVal) {
				vm.form.aniosModelo = [];
				query('Años', 'aniosModelo', {
					marcaId: vm.vehiculo.marca.id,
					modeloId: nVal.id
				});
			}
		});

		$scope.$watch('vm.vehiculo.anioModelo', nVal => {
			if (nVal) {
				getValorAsegurado();
			}
		});

		$scope.$watch('vm.vehiculo.departamento', nVal => {
			if (nVal) {
				vm.form.ciudades = [];
				query('Ciudades', 'ciudades', {
					departamentoId: nVal.id
				});
			}
		});

		/* PRIVATE */
		function onInit() {
			vm.form = {};
			query('Marcas', 'marcas');
			query('Departamentos', 'departamentos');
			query('PrendaVehicular', 'optPrendaVehicular');
			query('0km', 'opt0km');
		}

		function query(resource, model, query = {}) {
			const loadingKey = `loading${_.upperFirst(model)}`;
			$log.debug('READ ONLY', `readOnly${_.upperFirst(model)}`);
			if (!vm.form[`readOnly${_.upperFirst(model)}`]) {
				vm.form[loadingKey] = true;
				$request.socket
					.query(resource, query)
					.then(resources => {
						vm.form[model] = resources;
						assignVehiculo(model);
					})
					.catch(err => {
						$log.error(err);
						$toast.error(`No es posible obtener ${resource}`);
					})
					.finally(() => {
						vm.form[loadingKey] = false;
					});
			}
		}

		function getValorAsegurado() {
			vm.form.loadingValorAsegurado = true;
			$request.socket
				.plain('ValorAsegurado', {
					marcaId: vm.vehiculo.marca.id,
					modeloId: vm.vehiculo.modelo.id,
					anioId: vm.vehiculo.anioModelo.id
				})
				.then(valorAsegurado => {
					const valor = parseInt(valorAsegurado);
					vm.vehiculo.valor = {
						aseguradoMax: Math.round(valor * 1.1),
						aseguradoMin: Math.round(valor * 0.9),
						asegurado: valor
					};
					vm.vehiculo.valorAsegurado = valorAsegurado;
				})
				.catch(err => {
					$log.error(err);
					$toast.error('Hubo un problema obteniendo el valor asegurado');
				})
				.finally(() => {
					vm.form.loadingValorAsegurado = false;
				});
		}

		function setReadOnly(cotizaVehiculo) {
			$log.debug('cotizaVehiculo', cotizaVehiculo);
			vm.form.readOnlyMarcas = cotizaVehiculo.marca;
			vm.form.readOnlyModelos = cotizaVehiculo.modelo;
			vm.form.readOnlyAniosModelo = cotizaVehiculo.anioModelo;
		}

		function assignVehiculo(model) {
			if (model !== 'all') {
				let field = null;
				switch (model) {
					case 'marcas':
						field = 'marca';
						break;
					case 'departamentos':
						field = 'departamento';
						break;
					case 'ciudades':
						field = 'ciudad';
						break;
					case 'opt0km':
						field = 'vehiculo0km';
						break;
					case 'optPrendaVehicular':
						field = 'prendaVehicular';
						break;
				}
				if (field && vm.vehiculo) {
					if (
						vm.vehiculo[field] &&
						vm.vehiculo[field].id &&
						!vm.vehiculo[field].valor &&
						vm.form[model] &&
						vm.form[model].length > 0
					) {
						vm.vehiculo[field] = _.find(vm.form[model], {
							id: vm.vehiculo[field].id
						});
					}
				}
			} else {
				[
					{
						field: 'departamento',
						model: 'departamentos'
					},
					{
						field: 'ciudad',
						model: 'ciudades'
					},
					{
						field: 'vehiculo0km',
						model: 'opt0km'
					},
					{
						field: 'prendaVehicular',
						model: 'optPrendaVehicular'
					}
				].forEach(fieldModel => {
					const field = fieldModel.field;
					const model = fieldModel.model;
					if (field && vm.vehiculo) {
						if (
							vm.vehiculo[field] &&
							vm.vehiculo[field].id &&
							!vm.vehiculo[field].valor &&
							vm.form[model] &&
							vm.form[model].length > 0
						) {
							vm.vehiculo[field] = _.find(vm.form[model], {
								id: vm.vehiculo[field].id
							});
						}
					}
				});
			}
		}
	}
})();
