(() => {
	'use strict';

	const WIDGETS = [
		{
			name: 'publicidad',
			className: [ 'section-wrapper' ],
			attr: {
				src: 'campana-auto-noviembre.jpg',
				terminos: 'terminos_y_condiciones'
			}
		},
		{
			name: 'compra-steps',
			className: [ 'section-wrapper' ]
		},
		{
			name: 'beneficios-steps',
			className: [ 'section-wrapper' ]
		},
		{
			name: 'whatsapp',
			className: [ 'section-wrapper' ],
			attr: {
				src: 'contactus-whatsapp.jpg'
			}
		}
	];

	angular
		.module('app.pages')
		.controller('StepOneController', StepOneController);

	function StepOneController($log, $scope, $state, $steps, $widget, $pubsub) {
		/* CONSTANT */
		const vm = this;
		const STATE_NAME = $state.$current.name;
		const VIEWS_STORE = $pubsub.createStore('views', 1);
		const LOADING_STORE = $pubsub.createStore('loading', true);

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name !== STATE_NAME) {
				if ($steps.current() !== 1) {
					$steps.change(1);
				}
			}
		});

		/* PUBLIC */
		vm.step = 1;
		vm.widgets = $widget.get(WIDGETS);

		/* STORES */
		VIEWS_STORE.subscribe(nView => {
			vm.step = nView;
		});

		LOADING_STORE.subscribe(value => {
			vm.loading = value;
		});
	}
})();
