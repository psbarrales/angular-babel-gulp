describe('StepOneController', () => {
	let $controller;
	beforeEach(window.mockingModules);
	beforeEach(module('application'));

	beforeEach(
		inject((_$controller_, $rootScope) => {
			$controller = _$controller_('StepOneController', {
				$scope: $rootScope.$new()
			});
		})
	);

	it('Should StepOneController to be defined', () => {
		expect($controller).toBeDefined();
	});
});
