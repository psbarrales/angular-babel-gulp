(() => {
	'use strict';

	angular
		.module('app.pages')
		.config($stepsProvider => {
			$stepsProvider.add({
				active: true,
				weight: 1,
				title: 'Cotiza',
				description: 'Tus datos personales',
				state: 'steps.step-1'
			});
		})
		.config($stateProvider => {
			$stateProvider.state('steps.step-1', {
				url: '/step-1',
				views: {
					'': {
						controller: 'StepOneController',
						controllerAs: 'vm',
						templateUrl: 'app/pages/steps/step-1/step-1.tmpl.html'
					},
					'cotiza@steps.step-1': {
						controller: 'CotizaViewController',
						controllerAs: 'vm',
						templateUrl:
							'app/pages/steps/step-1/views/cotiza/cotiza.view.tmpl.html'
					},
					'vehiculo@steps.step-1': {
						controller: 'VehiculoViewController',
						controllerAs: 'vm',
						templateUrl:
							'app/pages/steps/step-1/views/vehiculo/vehiculo.view.tmpl.html'
					},
					'asegurado@steps.step-1': {
						controller: 'AseguradoViewController',
						controllerAs: 'vm',
						templateUrl:
							'app/pages/steps/step-1/views/asegurado/asegurado.view.tmpl.html'
					}
				}
			});
		});
})();
