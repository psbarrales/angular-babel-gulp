(() => {
	'use strict';

	angular.module('app.pages').factory('$stepOne', ($q, $log) => {
		const storage = {};
		const watchers = {};
		return {
			create: watch,
			set: set,
			get: get,
			getOne: getOne,
			remove: remove
		};

		function trigger(path, event) {
			$log.debug('Trigger', event, path);
			_.get(watchers, path).notify(_.get(storage, path));
		}
		function create(path) {
			$log.debug('Create', path);
			_.set(storage, path, null);
			watch(path);
		}
		function watch(path) {
			_.set(watchers, path, $q.defer());
		}
		function exist(path) {
			const _exist = _.get(storage, path);
			if (angular.isUndefined(_exist)) {
				create(path);
				trigger(path, 'create');
			}
		}

		function set(path, value) {
			exist(path);
			_.set(storage, path, value);
			trigger(path, 'change');
		}

		function get(path, cb) {
			exist(path);
			_.get(watchers, path).promise.then(null, null, cb);
			return angular.isFunction(cb) ? cb(_.get(storage, path)) : null;
		}

		function getOne(path, cb) {
			exist(path);
			return angular.isFunction(cb) ? cb(_.get(storage, path)) : null;
		}

		function remove(path) {
			_.unset(storage, path);
			_.unset(watchers, path);
		}
		/* return {
			on: on,
			one: one,
			event: event
		};

		function event(name, response) {
			(callbacks[name] || []).forEach(cb => {
				$localStorage.setItem(`cache.${name}`, response);
				cb(response);
			});
		}

		function on(name, cb) {
			callbacks[name] = callbacks[name] || [];
			callbacks[name].push(cb);
			const cache = $localStorage.getItem(`cache.${name}`);
			if (cache) {
				event(name, cache);
			}
		}

		function one(name, cb) {
			callbacks[name] = [];
			callbacks[name].push(cb);
			const cache = $localStorage.getItem(`cache.${name}`);
			if (cache) {
				event(name, cache);
			}
		} */
	});
})();
