(() => {
	'use strict';

	const WIDGETS = [
		{
			name: 'detalle-cotizacion',
			className: ['']
		}
	];

	angular.module('app.pages').controller('StepTwoController', StepTwoController);

	function StepTwoController($log, $q, $widget, $pubsub, $timeout, $request, $scope, $state, $steps, $stateParams) {
		const vm = this;
		const STATE_NAME = $state.$current.name;
		vm.widgets = $widget.get(WIDGETS);

		/* STORES */
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');
		const INFORMACIONVEHICULO_STORE = $pubsub.createStore('informacionVehiculo');

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name === STATE_NAME) {
				onInit();
			}
		});

		function onInit() {
			if ($steps.current() !== 2) {
				$steps.change(2);
			}
			if (!$stateParams.cotizacionId) {
				$log.debug('STATE', COTIZACION_STORE.getState());
				updatePlanes(COTIZACION_STORE.getState());
			} else {
				vm.loading = true;
				requestCotizacion($stateParams.cotizacionId).then(cotizacion => {
					getDatosVehiculo(cotizacion);
					vm.loading = false;
				});
			}
		}

		function updatePlanes(cotizacion) {
			if (cotizacion && cotizacion.plans) {
				const planesSinPrecio = _.filter(cotizacion.plans, plan => {
					return !plan.prima.bruta || !plan.prima.iva || !plan.prima.neta;
				});

				if (planesSinPrecio.length > 0) {
					$timeout(() => {
						requestCotizacion(cotizacion.id);
					}, 1000);
				}
			}
		}

		function requestCotizacion(cotizacion, promise) {
			const defer = promise || $q.defer();
			$request
				.get('Cotizacion', {
					cotizacionId: cotizacion
				})
				.then(cotizacion => {
					updatePlanes(cotizacion);
					COTIZACION_STORE.dispatch(cotizacion);
					defer.resolve(cotizacion);
				})
				.catch(err => {
					$log.error(err);

					if (vm.retry < 5) {
						$timeout(() => {
							vm.retry++;
							defer.promise = requestCotizacion(cotizacion, defer);
						}, 1500);
					} else {
						vm.retry = 0;
						defer.reject();
					}
				});
			return defer.promise;
		}

		function getDatosVehiculo(cotizacion) {
			$q
				.all([
					$request.query('Marca', {
						marcaId: cotizacion.insured.marca.id
					}),
					$request.query('Modelo', {
						marcaId: cotizacion.insured.marca.id,
						modeloId: cotizacion.insured.modelo.id
					})
				])
				.then(results => {
					INFORMACIONVEHICULO_STORE.dispatch({
						marca: results[0][0],
						modelo: results[1][0],
						anioModelo: {
							id: cotizacion.insured.anioModelo.id,
							valor: cotizacion.insured.anioModelo.id
						}
					});
				});
		}
	}
})();
