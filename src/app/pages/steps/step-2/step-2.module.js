(() => {
	'use strict';

	angular
		.module('app.pages')
		.config($stepsProvider => {
			$stepsProvider.add({
				weight: 2,
				title: 'Compara',
				description: 'Elige el mejor seguro',
				state: 'steps.step-2'
			});
		})
		.config($stateProvider => {
			$stateProvider.state('steps.step-2', {
				url: '/step-2/:cotizacionId',
				views: {
					'': {
						controller: 'StepTwoController',
						controllerAs: 'vm',
						templateUrl: 'app/pages/steps/step-2/step-2.tmpl.html'
					}
				}
			});
		});
})();
