(() => {
	'use strict';

	angular
		.module('app.component')
		.component('caluga', {
			templateUrl: 'app/pages/steps/components/matriz/caluga.tmpl.html',
			controller: CalugaController,
			bindings: {
				plan: '='
			}
		})
		.component('calugaLoading', {
			templateUrl:
				'app/pages/steps/components/matriz/caluga-loading.tmpl.html'
		});

	function CalugaController(
		$log,
		$scope,
		$mdDialog,
		$pubsub,
		$steps,
		$request
	) {
		const $ctrl = this;
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');
		const PLAN_STORE = $pubsub.createStore('plan');

		$ctrl.rceModal = rceModal;
		$ctrl.assistance = assistance;
		$ctrl.benefits = benefits;
		$ctrl.otrasTarjetas = otrasTarjetas;
		$ctrl.pagarCMR = pagarCMR;
		$ctrl.llamar = llamar;
		$ctrl.coverage = false;

		$ctrl.$onChanges = () => {
			$ctrl.price = calculatePrices($ctrl.plan);
			$ctrl.details = details([
				'CONDUCTOR ELEGIDO',
				'GRÚA',
				'VEHÍCULO REEMPLAZO'
			]);
			$ctrl.rce = rce('RESPONSABILIDAD CIVIL EXTRA');
			$ctrl.deductibles = deductibles();
		};

		function calculatePrices(plan) {
			if (plan.prima && plan.prima.neta) {
				return {
					annual: Math.round(plan.prima.neta),
					month: Math.round(plan.prima.neta / 12)
				};
			}
		}

		function details(detalles) {
			return detalles.map(detalle => {
				return detail(detalle);
			});
		}

		function detail(alias) {
			const detalle = _.find($ctrl.plan.detalles, {
				etiqueta: alias
			});
			if (detalle) {
				return {
					name: _.capitalize(alias),
					value: detalle.valor === 'SI'
				};
			} else {
				return false;
			}
		}

		function rce(alias) {
			const detalle = _.find($ctrl.plan.detalles, {
				etiqueta: alias
			});
			if (detalle) {
				return parseInt(detalle.valor.match(/\d+/g));
			} else {
				return 0;
			}
		}

		function deductibles() {
			if ($ctrl.plan && $ctrl.plan.deducibles) {
				return $ctrl.plan.deducibles.map(deductible => {
					return {
						name: _.capitalize(deductible.etiqueta),
						value: _.capitalize(deductible.valor)
					};
				});
			} else {
				return [];
			}
		}

		/* ACTIONS */
		function otrasTarjetas() {
			window.location.href = `${$request.endpoint(
				'AntiguaExperiencia'
			)}${COTIZACION_STORE.getState().cotizacion}&sp=${$ctrl.plan
				.subProducto.id}`;
		}

		function pagarCMR() {
			PLAN_STORE.dispatch($ctrl.plan);
			$steps.change(3);
		}

		function llamar() {
			window.open(
				'https://contact.falabella.com/WebAPI812/SEGUROS_CO/WebCallback/CTC/WebCallback/formulario.html?url_actual=MatrizNEXD',
				'Click to Call',
				'width=482,height=645,scrollbars=NO'
			);
		}

		/* MODAL */
		function rceModal($event) {
			const parentEl = angular.element(document.body);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: true,
				escapeToClose: true,
				disableParentScroll: true,
				template:
					'<md-dialog class="rce-dialog">' +
					'  <md-dialog-content>' +
					'<div>Este es el valor que te ayudara a cubrir los daños hechos a terceros, cuando tu Auto se encuentre involucrado en un accidente.</div>' +
					'  </md-dialog-content>' +
					'</md-dialog>'
			});
		}

		function assistance($event) {
			const parentEl = angular.element(document.body);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: true,
				escapeToClose: true,
				disableParentScroll: true,
				templateUrl:
					'app/pages/steps/components/matriz/assistance.dialog.tmpl.html'
			});
		}

		function benefits($event) {
			const parentEl = angular.element(document.body);
			const scope = $scope.$new();
			scope.$ctrl = angular.copy($ctrl);
			$mdDialog.show({
				parent: parentEl,
				targetEvent: $event,
				clickOutsideToClose: true,
				escapeToClose: true,
				disableParentScroll: true,
				scope: scope,
				controller: BenefitsModalController,
				controllerAs: '$modal',
				templateUrl:
					'app/pages/steps/components/matriz/benefits.dialog.tmpl.html'
			});
		}
	}

	function BenefitsModalController($log, $scope, $request, $pubsub, $toast) {
		const $modal = this;
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');

		$modal.loading = true;

		parseCotizacion(COTIZACION_STORE.getState());
		getPlan();

		function parseCotizacion(cotizacion) {
			$modal.cotizacionId = cotizacion.cotizacion;
			$modal.fechaCotizacion = cotizacion.fechaCotizacion;
		}

		function filter(item, yes) {
			if (yes) {
				return (
					item.valor.toUpperCase() !== 'NO' &&
					item.valor.toUpperCase() !== 'NO TIENE ESTA COBERTURA'
				);
			} else {
				return (
					item.valor.toUpperCase() === 'NO' ||
					item.valor.toUpperCase() === 'NO TIENE ESTA COBERTURA'
				);
			}
		}

		function parsePlan(plan) {
			return {
				benefits: {
					has: plan.beneficios.filter(item => filter(item, true)),
					hasnt: plan.beneficios.filter(item => filter(item, false))
				},
				coverage: {
					has: plan.coberturas.filter(item => filter(item, true)),
					hasnt: plan.coberturas.filter(item => filter(item, false))
				}
			};
		}

		function getPlan() {
			$request
				.get('Plan', {
					planId: $scope.$ctrl.plan.subProducto.id
				})
				.then(plan => {
					$modal.plan = parsePlan(plan);
				})
				.catch(err => {
					$log.error(err);
					$toast.error(
						`No es posible obtener los beneficios y coberturas de este plan`
					);
				})
				.finally(() => {
					$modal.loading = false;
				});
		}
	}
})();
