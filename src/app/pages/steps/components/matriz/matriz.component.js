(() => {
	'use strict';

	angular.module('app.component').component('matriz', {
		template: `<caluga ng-repeat="plan in $ctrl.planes" plan="plan"></caluga>
			<caluga-loading ng-if="$ctrl.waitPlanes"></caluga-loading>`,
		controller: MatrizController
	});

	function MatrizController($log, $pubsub) {
		const $ctrl = this;
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');

		$ctrl.planes = parsePlanes(COTIZACION_STORE.getState());

		function parsePlanes(cotizaciones) {
			if (cotizaciones && cotizaciones.plans) {
				const planesVisibles = cotizaciones.plans.filter(plan => {
					return (
						plan.prima.bruta && plan.prima.iva && plan.prima.neta
					);
				});
				$ctrl.waitPlanes =
					cotizaciones.plans.length !== planesVisibles.length;
				return planesVisibles;
			}
		}

		COTIZACION_STORE.subscribe(cotizaciones => {
			$ctrl.planes = parsePlanes(cotizaciones);
		});
	}
})();
