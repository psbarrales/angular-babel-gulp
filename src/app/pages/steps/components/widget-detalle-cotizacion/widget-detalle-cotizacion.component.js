(() => {
	'use strict';

	angular.module('app.component').component('widgetDetalleCotizacion', {
		restrict: 'E',
		templateUrl:
			'app/pages/steps/components/widget-detalle-cotizacion/widget-detalle-cotizacion.tmpl.html',
		bindings: {},
		controller: WidgetDetalleCotizacionController
	});

	function WidgetDetalleCotizacionController($log, $pubsub) {
		const $ctrl = this;
		const COTIZACION_STORE = $pubsub.createStore('cotizaciones');
		const INFORMACIONVEHICULO_STORE = $pubsub.createStore(
			'informacionVehiculo'
		);

		COTIZACION_STORE.subscribe(cotizacion => {
			parseDetalle(cotizacion);
		});

		INFORMACIONVEHICULO_STORE.subscribe(informacionVehiculo => {
			parseVehiculo(informacionVehiculo);
		});

		function parseDetalle(cotizacion) {
			if (cotizacion && cotizacion.insured) {
				const vehiculo = cotizacion.insured;
				$ctrl.valAsegurado = vehiculo.valAsegurado;
				$ctrl.fechaCotizacion = cotizacion.created;
				$ctrl.placa = vehiculo.placa;
			}
		}

		function parseVehiculo(vehiculo) {
			if (
				vehiculo &&
				vehiculo.marca &&
				vehiculo.modelo &&
				vehiculo.anioModelo
			) {
				$ctrl.vehiculo = '';
				$ctrl.vehiculo += vehiculo.marca.valor;
				$ctrl.vehiculo += ' ' + vehiculo.modelo.valor;
				$ctrl.vehiculo += ', ' + vehiculo.anioModelo.valor;
			}
		}

		parseDetalle(COTIZACION_STORE.getState());
		parseVehiculo(INFORMACIONVEHICULO_STORE.getState());
	}
})();
