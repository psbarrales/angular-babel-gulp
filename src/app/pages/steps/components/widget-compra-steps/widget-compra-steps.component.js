(() => {
	'use strict';

	angular.module('app.component').component('widgetCompraSteps', {
		templateUrl:
			'app/pages/steps/components/widget-compra-steps/widget-compra-steps.tmpl.html'
	});
})();
