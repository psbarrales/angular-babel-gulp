(() => {
	'use strict';

	angular.module('app.component').component('widgetDetallePlan', {
		restrict: 'E',
		templateUrl:
			'app/pages/steps/components/widget-detalle-plan/widget-detalle-plan.tmpl.html',
		bindings: {},
		controller: WidgetDetallePlanController
	});

	function WidgetDetallePlanController($pubsub) {
		const $ctrl = this;

		const PLAN_STORE = $pubsub.createStore('plan');
		const INFORMACIONVEHICULO_STORE = $pubsub.createStore(
			'informacionVehiculo'
		);

		function parsePlan(plan) {
			if (plan && plan.prima) {
				$ctrl.plan = {
					compania: plan.compania,
					month: Math.round(plan.prima.neta / 12),
					annual: Math.round(plan.prima.neta)
				};
			}
		}

		function parseVehiculo(vehiculo) {
			if (
				vehiculo &&
				vehiculo.marca &&
				vehiculo.modelo &&
				vehiculo.anioModelo
			) {
				$ctrl.vehiculo = '';
				$ctrl.vehiculo += vehiculo.marca.valor;
				$ctrl.vehiculo += ' ' + vehiculo.modelo.valor;
				$ctrl.vehiculo += ', ' + vehiculo.anioModelo.valor;
			}
		}

		INFORMACIONVEHICULO_STORE.subscribe(informacionVehiculo => {
			parseVehiculo(informacionVehiculo);
		});

		PLAN_STORE.subscribe(plan => {
			parsePlan(plan);
		});

		parsePlan(PLAN_STORE.getState());
		parseVehiculo(INFORMACIONVEHICULO_STORE.getState());
	}
})();
