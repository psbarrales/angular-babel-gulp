(() => {
	'use strict';

	angular.module('app.component').component('widgetPublicidad', {
		restrict: 'E',
		template:
			'<div><img ng-src="{{$ctrl.src | imageUrl }}" /><br />' +
			'<div class="terminos"><a target="_blank" ng-href="{{$ctrl.terminos | url}}">' +
			'Ver términos y condiciones</a></div></div>',
		bindings: {
			src: '@',
			terminos: '@'
		}
	});
})();
