(() => {
	'use strict';

	angular.module('app.component').component('widgetWhatsapp', {
		restrict: 'E',
		template: '<img ng-src="{{$ctrl.src | imageUrl }}" />',
		bindings: {
			src: '@'
		}
	});
})();
