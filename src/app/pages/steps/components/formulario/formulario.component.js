(() => {
	'use strict';

	angular
		.module('app.component')
		.component('formulario', {
			templateUrl:
				'app/pages/steps/components/formulario/formulario.tmpl.html',
			bindings: {
				configuration: '='
			},
			controller: FormularioController
		})
		.component('field', {
			templateUrl:
				'app/pages/steps/components/formulario/field.tmpl.html',
			bindings: {
				config: '='
			},
			controller: FieldController,
			controllerAs: '$field'
		});

	function FormularioController($log, $pubsub) {
		const $ctrl = this;
		const FORMULARIO_STORE = $pubsub.createStore('formularios');
		const FORMDINAMICO_STORE = $pubsub.createStore('formularioDinamico');

		$ctrl.campos = [];
		$ctrl.editForm = editForm;
		$ctrl.submitForm = submitForm;

		function editForm(formulario) {
			$ctrl.formActivo = formulario.formularioid;
		}

		function submitForm(formulario, form) {
			/* PROCESS DATA */
			formulario.campos.forEach(campo => {
				assignCampo(campo, form[_.camelCase(campo.nombre)].$modelValue);
			});
			nextForm(formulario);
		}

		function assignCampo(campo, value) {
			const i = _.findIndex($ctrl.campos, {
				campoid: campo.campoid
			});

			$ctrl.campos[i].valor = value.id || value;
		}

		function nextForm(formulario) {
			/* NEXT FORM */
			const form = _.find(
				$ctrl.formularios,
				form => form.formularioid > formulario.formularioid
			);
			if (form) {
				$ctrl.formActivo = form.formularioid;
			} else {
				FORMDINAMICO_STORE.dispatch($ctrl.campos);
				/* END! */
			}
		}

		$ctrl.$onInit = () => {
			const formularios = FORMULARIO_STORE.getState();
			if ($ctrl.configuration && formularios) {
				$ctrl.formularios = formularios;
				parseFormulario();
			}
		};

		FORMULARIO_STORE.subscribe(formularios => {
			if ($ctrl.configuration && formularios) {
				$ctrl.formularios = formularios;
				parseFormulario();
			}
		});

		function parseFormulario() {
			if ($ctrl.formularios) {
				const formularios = _.filter($ctrl.formularios, formulario => {
					return _.find($ctrl.configuration, {
						alias: formulario.alias
					});
				});
				$ctrl.formulario = formularios.map(getFormularios);
			}
		}

		function getFormularios(formulario) {
			if (!$ctrl.formActivo) {
				$ctrl.formActivo = formulario.formularioid;
			}
			return {
				formularioid: formulario.formularioid,
				alias: _.find($ctrl.configuration, { alias: formulario.alias })
					.alias,
				title: _.find($ctrl.configuration, { alias: formulario.alias })
					.title,
				campos: getCampos(getGrupos(formulario.grupos)),
				raw: formulario
			};
		}

		function getGrupos(grupos) {
			return _.filter(grupos, grupo => {
				return grupo.campos.length > 0;
			});
		}

		function getCampos(grupos) {
			const campos = [];
			grupos.forEach(grupo => {
				grupo.campos.forEach(campo => {
					$ctrl.campos.push({
						campoid: campo.campoid,
						codigorector: campo.codigorector,
						codigorectorsecundario: campo.codigorectorsecundario,
						nombre: campo.nombre,
						obligatorio: campo.obligatorio,
						regex: campo.regex,
						valor: campo.valor
					});
					if (campo.mostrar) {
						campos.push(campo);
					}
				});
			});
			return campos;
		}
	}

	function FieldController($log, $scope, $pubsub, $request) {
		const $field = this;
		let FIELD_STORE;

		$scope.fields = {};

		$scope.$watch(
			'fields',
			(nVal, oVal) => {
				if (nVal !== oVal) {
					FIELD_STORE.dispatch(nVal[_.first(Object.keys(nVal))]);
				}
			},
			true
		);

		$field.$onChanges = () => {
			$field.name = _.camelCase($field.config.nombre);
			$scope.fields[$field.name] = $field.config.valor || null;
			angular.extend($field, $field.config);

			FIELD_STORE = $pubsub.createStore(`campo${$field.campoid}`);

			urlExtraccion();
		};

		function urlExtraccion() {
			if ($field.urlextraccion) {
				if ($field.urlextraccion.indexOf('{') < 0) {
					$scope.loading = true;
					$request
						.http('FormCustom', $field.urlextraccion)
						.then(results => {
							$scope.options = results.data;
						})
						.finally(() => {
							$scope.loading = false;
						});
				}
			}
			if ($field.idcampodependencia) {
				const DEPS_STORE = $pubsub.createStore(
					`campo${$field.idcampodependencia}`
				);
				DEPS_STORE.subscribe(value => {
					$scope.loading = true;
					$scope.fields[_.first(Object.keys($scope.fields))] = null;
					if ($field.urlextraccion.indexOf('{') >= 0) {
						const url = $field.urlextraccion.replace(
							`{${$field.idcampodependencia}}`,
							value.id
						);
						$request
							.http('FormCustom', url)
							.then(results => {
								$scope.options = results.data;
							})
							.finally(() => {
								$scope.loading = false;
							});
					}
				});
			}
		}
	}
})();
