(() => {
	'use strict';

	angular.module('app.component').component('widgetBeneficiosSteps', {
		templateUrl:
        'app/pages/steps/components/widget-beneficios-steps/widget-beneficios-steps.tmpl.html'
	});
})();
