(() => {
	'use strict';

	angular.module('app.pages').controller('PagoFracasoController', PagoFracasoController);

	function PagoFracasoController($log, $state, $stateParams, $scope, $pubsub) {
		const vm = this;
		const STATE_NAME = $state.$current.name;
		const PAGO_STORE = $pubsub.createStore('pago');

		vm.repeatPay = repeatPay;

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name === STATE_NAME) {
				onInit();
			}
		});

		function onInit() {
			const error = PAGO_STORE.getState();

			if (error.error) {
				$log.debug(error);
				if (error.message && error.message.data && error.message.data.message) {
					vm.reason = (error.message.data.message || 'un error desconocido').toLowerCase();
				} else {
					vm.reason = 'que no se pudo completar la petición';
				}
			} else {
				vm.reason = 'un error desconocido';
				$log.debug('No hay error encontrado, redirigiendo al home...');
				// $state.go('steps.step-1');
			}
		}

		function repeatPay() {
			$state.go('steps.pago', { propuestaId: $stateParams.propuestaId });
		}
	}
})();
