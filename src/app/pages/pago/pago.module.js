(() => {
	'use strict';

	angular.module('app.pages').config($stateProvider => {
		$stateProvider.state('steps.pago', {
			url: '/pago/:propuestaId',
			views: {
				'': {
					controller: 'PagoController',
					controllerAs: 'vm',
					templateUrl: 'app/pages/pago/pago.tmpl.html'
				}
			}
		});
		$stateProvider.state('pago.exito', {
			url: '/:propuestaId/exito',
			views: {
				'': {
					controller: 'PagoExitoController',
					controllerAs: 'vm',
					templateUrl: 'app/pages/pago/views/exito/exito.tmpl.html'
				}
			}
		});
		$stateProvider.state('pago.fracaso', {
			url: '/:propuestaId/fracaso',
			views: {
				'': {
					controller: 'PagoFracasoController',
					controllerAs: 'vm',
					templateUrl: 'app/pages/pago/views/fracaso/fracaso.tmpl.html'
				}
			}
		});
	});
})();
