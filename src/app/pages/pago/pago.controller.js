(() => {
	'use strict';

	const WIDGETS = [
		{
			name: 'publicidad',
			className: ['section-wrapper'],
			attr: {
				src: 'campana-auto-noviembre.jpg',
				terminos: 'terminos_y_condiciones'
			}
		},
		{
			name: 'compra-steps',
			className: ['section-wrapper']
		},
		{
			name: 'beneficios-steps',
			className: ['section-wrapper']
		}
	];

	angular.module('app.pages').controller('PagoController', PagoController);

	function PagoController($log, $pubsub, $q, $request, $scope, $state, $stateParams, $steps, $toast, $widget) {
		const vm = this;
		const STATE_NAME = $state.$current.name;

		const PROPUESTA_STORE = $pubsub.createStore('propuesta');
		const PAGO_STORE = $pubsub.createStore('pago');

		vm.widgets = $widget.get(WIDGETS);
		vm.pagar = pagar;

		$scope.$on('$stateChangeSuccess', (ev, state) => {
			if (state.name === STATE_NAME) {
				onInit();
			}
		});

		function onInit() {
			if ($steps.current() !== 3) {
				$steps.change(3);
			}
			vm.pago = {
				propuesta: 0,
				aceptacion: {},
				tarjeta: {},
				mediopago: {
					cuotas: 12,
					id: 1
				}
			};
			readPropuesta();
		}

		function readPropuesta() {
			const propuesta = PROPUESTA_STORE.getState();
			let propuestaId;
			if (propuesta && propuesta.id) {
				propuestaId = propuesta.id;
			} else if ($stateParams && $stateParams.propuestaId) {
				propuestaId = $stateParams.propuestaId;
			}
			if (propuestaId) {
				vm.loading = true;
				vm.propuestaId = propuestaId;
				$q.all([getPropuesta(propuestaId), getMediosPago(propuestaId)]).finally(() => {
					vm.loading = false;
				});
			}
		}

		function getPropuesta(id) {
			const defer = $q.defer();
			$request
				.get('PropuestaResumen', {
					propuestaId: id
				})
				.then(propuesta => {
					vm.propuesta = propuesta;
					vm.pago.propuesta = id;
					vm.pago.tarjeta.nombre = propuesta.asegurado;
					vm.monthly = Math.round(propuesta.primaAnual / 12);
					defer.resolve(propuesta);
				})
				.catch(err => {
					$log.error(err);
					$toast.error(`No es posible obtener la propuesta ${id}`);
				});
			return defer.promise;
		}

		function getMediosPago(id) {
			const defer = $q.defer();
			$request
				.query('PropuestaMedioPago', {
					propuestaId: id
				})
				.then(mediospago => {
					vm.mediospago = mediospago;
					defer.resolve(mediospago);
				})
				.catch(err => {
					$log.error(err);
					$toast.error(`No es posible obtener los medios de pago para ${id}`);
					defer.reject(err);
				});
			return defer.promise;
		}

		function pagar() {
			vm.loading = true;
			const propuesta = PROPUESTA_STORE.getState();

			if (vm.pago.aceptacion.declaracion) {
				vm.pago.aceptacion.declaracion = 'Declaro que la información registrada es verdadera.';
			}

			if (vm.pago.aceptacion.terminos) {
				vm.pago.aceptacion.terminos = 'He leído y acepto los términos y condiciones';
			}

			$request
				.save('Pagar', vm.pago)
				.then(result => {})
				.catch(err => {
					$log.error(err);
					$toast.error('No es posible procesar el pago en estos momentos, Intente más tarde');
					PAGO_STORE.dispatch({
						error: true,
						message: err
					});
					$state.go('pago.fracaso', { propuestaId: vm.propuestaId });
				});
		}
	}
})();
