(() => {
	'use strict';

	angular.module('application').config($mdThemingProvider => {
		$mdThemingProvider.definePalette('segurosgreen', {
			'50': 'e6f6e6',
			'100': 'c1e8c1',
			'200': '98d898',
			'300': '6ec86f',
			'400': '4fbd50',
			'500': '30b131',
			'600': '2baa2c',
			'700': '24a125',
			'800': '1e981f',
			'900': '138813',
			A100: '30b131',
			A200: '30b131',
			A400: '30b131',
			A700: '30b131',
			contrastDefaultColor: 'light',
			contrastDarkColors: [
				'50',
				'100',
				'200',
				'300',
				'400',
				'A100',
				'A200',
				'A400',
				'A700'
			],
			contrastLightColors: [ '500', '600', '700', '800', '900' ]
		});

		$mdThemingProvider
			.theme('SegurosFalabella')
			.primaryPalette('segurosgreen')
			.accentPalette('segurosgreen')

		$mdThemingProvider.setDefaultTheme('SegurosFalabella');
	});
})();
