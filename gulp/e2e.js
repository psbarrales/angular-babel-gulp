'use strict';

import path from 'path';
import gulp from 'gulp';
import nightwatch from 'gulp-nightwatch';

gulp.task('e2e', [ 'serve' ], () => {
	return gulp.src('').pipe(
		nightwatch({
			configFile: 'test/nightwatch.json',
			cliArgs: []
		})
	);
});
