const fs = require('fs');
const exec = require('child_process').exec;

try {
	var ENV = process.env.ENV || 'staging';
	var scriptsFolder = __dirname + '/scripts';
	fs.readdirSync(scriptsFolder).forEach(file => {
		var wordToFind = 'environment="';
		var content = fs.readFileSync(scriptsFolder + '/' + file, 'utf-8');
		var index = content.indexOf(wordToFind);
		if (index >= 0) {
			var init = index + wordToFind.length;
			var end = content.indexOf('"', init);
			var currentProfile = content.substring(index, end);
			content = content.replace(
				currentProfile + '"',
				wordToFind + ENV + '"'
			);
		}
		fs.writeFileSync(scriptsFolder + '/' + file, content);
	});

	var mapsFolder = __dirname + '/maps/scripts';
	fs.readdirSync(mapsFolder).forEach(file => {
		var wordToFind = 'environment= \\"';
		var content = fs.readFileSync(mapsFolder + '/' + file, 'utf-8');
		var index = content.indexOf(wordToFind);
		if (index >= 0) {
			var init = index + wordToFind.length;
			var end = content.indexOf('\\"', init);
			var currentProfile = content.substring(index, end);
			content = content.replace(
				currentProfile + '\\"',
				wordToFind + ENV + '\\"'
			);
		}
		fs.writeFileSync(mapsFolder + '/' + file, content);
	});
} catch (err) {
	console.log('something went wrong', err);
}
