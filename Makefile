image:
	docker build -t web-co-sf-auto:dev .
prepare:
	yarn && bower install
serve:
	docker run -it --rm -v $(shell pwd)/src:/workspace/src -p 3000:3000 -p 3001:3001 web-co-sf-auto:dev
unit-test:
	docker run -it --rm -v $(shell pwd)/src:/workspace/src -v $(shell pwd)/dist:/workspace/dist web-co-sf-auto:dev gulp test
docs:
	docker run -it --rm -v $(shell pwd)/src:/workspace/src -v $(shell pwd)/docs:/workspace/docs web-co-sf-auto:dev jsdoc . -c jsdoc.json -r -d docs --verbose 
dist:
	make image
	mkdir -p dist
	docker run -it --rm -v $(shell pwd)/src:/workspace/src -v $(shell pwd)/dist:/workspace/dist web-co-sf-auto:dev gulp build
	docker build -t web-co-sf-auto:latest . -f Dockerfile.nginx
	rm -rf dist
lift:
	docker run -it --rm -p 3000:80 web-co-sf-auto:latest
